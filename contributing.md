How to use project linters: 

pip install pre-commit

After that any commit will be automatically checked and reformatted by instructions from .pre-commit-config.yaml file.
After reformating please add to staged reformatted files (if any) and commit new reformatted files.
